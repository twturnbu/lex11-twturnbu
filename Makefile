
OS := $(shell uname -s)

ifeq ($(OS), Darwin) 
  CUNIT_PATH_PREFIX = /usr/local/Cellar/cunit/2.1-3/
  CUNIT_DIRECTORY = cunit
endif
ifeq ($(OS), Linux) 
  CUNIT_PATH_PREFIX = /util/CUnit/
  CUNIT_DIRECTORY = CUnit/
endif

CC = gcc
FLAGS = -g -Wall -std=c11

Date.o: Date.c
	$(CC) -c $(FLAGS) Date.c

Date: Date.o
	$(CC) $(FLAGS) -o program program.o

Runner: Runner.c Date.o
	$(CC) $(FLAGS) -o Runner Date.o Runner.c

test.o: test.c
	$(CC) -c $(FLAGS) -I $(CUNIT_PATH_PREFIX)include/$(CUNIT_DIRECTORY) test.c

tests: Date.o test.o
	gcc -Wall -L $(CUNIT_PATH_PREFIX)lib -I $(CUNIT_PATH_PREFIX)include/$(CUNIT_DIRECTORY) -o tests Date.o test.o -lcunit

clean: 
	rm -rf *~ *.o Date tests Runner *.dSYM

