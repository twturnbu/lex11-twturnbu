#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "CUnit.h"
#include "Basic.h"
#include "Date.h"

void runTest(int, int, int, int, int, int);

// Given input January 1st 1980, returns January 2nd 1980
void test01(void) {runTest(1,1,1980, 1,2,1980); }
void test02(void) {runTest(3,4,1767, 3,5,1767); }


void runTest(int inDay, int inMonth, int inYear, int expDay, int expMonth, int expYear){

  struct Date * today;
  today = (struct Date *) malloc(sizeof(today));
  (*today).date = inDay;
  (*today).month = inMonth;
  (*today).year = inYear;

  struct Date * actual = nextDay(today);
  printf("INPUT: \"%d\", \"%d\", \"%d\" EXPECTED: \"%d\", \"%d\", \"%d\" ACTUAL: \"%d\", \"%d\", \"%d\"...", 
     inDay, inMonth, inYear, expDay, expMonth, expYear, (*actual).date, (*actual).month, (*actual).year);

  CU_ASSERT_EQUAL((*actual).date, expDay);
  CU_ASSERT_EQUAL((*actual).month, expMonth);
  CU_ASSERT_EQUAL((*actual).year, expYear);

}

int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", NULL, NULL);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   if (
          (NULL == CU_add_test(pSuite, "test01", test01))
	|| (NULL == CU_add_test(pSuite, "test02", test02))    
  )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   //   CU_basic_show_failures(CU_get_failure_list());
   CU_cleanup_registry();
   return CU_get_error();
 }
   
