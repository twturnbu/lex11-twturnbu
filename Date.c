#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Date.h"

struct Date * nextDay(struct Date * today){

bool isLeapYear;

if((*today).year % 4 != 0){
  isLeapYear = false;
}
else if((*today).year % 100 != 0){
  isLeapYear = true;
}
else if((*today.year % 400 != 0){
  isLeapYear = false;
}
else {
  isLeapYear = true;
}


  struct Date * tomorrow;
  tomorrow = (struct Date *) malloc(sizeof(tomorrow));
  (*tomorrow).date = 0;
  (*tomorrow).month = 0;
  (*tomorrow).year = 0;

  return tomorrow;

}
